\begin{figure*}[!ht]
\begin{center}
\includegraphics[scale=0.4]{tex/figures/dv_ht45.pdf}
 \caption{\textit{Blue}: Solvent quantities; \textit{Red}: Cell quantities; \textit{Black}: mass weighted averaged solvent and cell quantities. \textbf{(A,E)} Mass density along the radial direction. \textbf{(B,F)} Normalized azimuthial velocity with respect to the theoretical maximum velocity. \textbf{(C,G)} Normalized azimuthal momentum. \textbf{(D,H)} Shear rate obtained by numerical differentiation of the average particle velocity, converted to physical units (SI).}
 \label{fig:vel}
 \end{center}
\end{figure*}

\subsection{Impact of cell concentration}
The results for different volume fractions of cells, $Ht=15\%$ and $Ht=45\%$, are presented below.
% density
Figs.~\ref{fig:vel}(A,E) show the average particle density along the radial direction.
The total density of the system (black line) is uniform in the interior of the domain and has small oscillations on the edges, as a result of the space confinement, due to the presence of the walls.
By comparing the density profile for the DPD (blue line) and the cell (red line) particles, it is seen that the DPD particle density increases with the distance from the wall.
In contrary, the cell's density decreases close to the wall.
This results in the creation of a ``cell-free'' layer (CFL) next to the cylindrical wall.
The CFL has been observed in \textit{in-vitro} experiments of blood flow through microvessels~\cite{FedosovDA2012}.
This is the origin of the Fahraeus-Lindqvist effect, which predicts an increase in the apparent viscosity with increasing vessel diameter, due to the lower viscosity of the CFL in comparison with the cell core layer.
In simulations the decrease in the cell density is balanced by an increase in the DPD particle density.

% momentum + comparizon to DPD only/analytical sln
Fig.~\ref{fig:vel}(C,G) depicts the normalized momentum distributions in the system.
The presence of the cells changes significantly the qualitative behavior of the total momentum distribution.
This reveals the highly non-Newtonian behavior of blood.
We believe that this difference is a direct consequence of the CFL, which causes a redistribution of the RBCs inside the domain, leading to regions with high viscosity contrasts.
% shear rate
High viscosity contrast leads to the adjustment of the shear rate profile (Fig.~\ref{fig:vel}(D,H)), such that the total shear stress per ring is conserved.
The physical range of shear rates covered in simulations is $3.5 - 27\,sec^{-1}$ (Fig.~\ref{fig:vel}(C,F)), which results in an increase of 7 times along the radial direction, consistent with the theoretical expectations.

\subsection{RBC microstructure characteristics}

\begin{figure*}[t!]
\begin{center}
  \includegraphics[scale=0.43]{tex/figures/microB_all.pdf}
 \caption{Cell's configuration: Averaged quantities (bending energy, asphericity and orientation angle) along the radial direction. The filled dots are data values, the black line is the mean value and the edges of the gray area around the mean value represent the standard deviation. The dots are colored according to the probability density function computed from all data points (regardless of the radial position), where white is high, and black is low point density.}
\label{fig:configs}
\end{center}
\end{figure*}

We now focus on the characterization of the cells' configuration.
In Fig.~\ref{fig:configs} we show bending energy~(\ref{eq:bending}), asphericity and orientation angle.
Four different simulation cases are shown, with high ($45\%$) and low ($15\%$) hematocrits, for high ($\gamma_{ss}=200$) and low ($\gamma_{ss}=8$) viscosity of the solvents.
The bending energy and the asphericity is a metric of the deformation that the membrane is subjected to, compared to the equilibrium state.
At high solvent viscosity, there is no large qualitative difference in the $Ht=15\%$ and $Ht=45\%$ cases.
However, at low solvent viscosity the cell concentration plays an important role in distribution of all three metrics presented, especially in the region near the inner wall, where higher shear stresses are experienced.
This suggests that cells are undergoing larger deformations (high bending energy and asphericity), and have a stronger alignment (small variation in orientation) at the proximity of the inner wall.

% RBC tracking in time:
We also present the time evolution of the membrane's bending energy after a steady state flow is reached.
The mean value of the autocorrelation in the bending energies of randomly selected cells lying in the first $10\%$ of the distance for inner wall is shown in Fig.~\ref{fig:intime} (\textit{left}).
By comparing the $Ht = 15\%$ and $Ht = 45\%$ cases, it is observed that the autocorrelation peaks have a lower magnitude in the high concentration case.
We therefore propose that the concentration affects the regularity of the cell behavior, suggesting that it becomes more chaotic.
The variations of the bending energy of a selected cell are mapped to its shape in Fig.~\ref{fig:intime}.
The low bending energy values correspond to a relaxed shape, while the highest bending energies occur during the rotation of the cell's membrane around itself (tank-treading).

\begin{figure*}[t!]
\begin{center}
  \includegraphics[scale=0.46]{tex/figures/intime_BE_auto+1.pdf}
 \caption{The bending energy for the membrane of one RBC in time. The circles correspond to the RBC snapshots shown in the left. The RBC stays in the first $1/10^{th}$ of the radial distance.}
\label{fig:intime}
\end{center}
\end{figure*}
