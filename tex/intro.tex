\section{Introduction} \label{sec:intro}
% A. What is the problem to be solved? Significance/the "why": practical applications in medicine
Blood transport is fundamental process in the human body functionality and has attracted numerous clinically relevant studies throughout the years.
The transport properties and rheology of blood are governed in principle by mechanical and hydrodynamic interactions of the red blood cells (RBCs), with neighboring RBCs and the surrounding plasma.
In spite of remarkable advances in the field of hemorheology, experiments are limited regarding the type and detail of microstructural information they can provide.
The acquisition of information for blood microrheology, including complex phenomena occurring at the microscale remains a compelling task.
Simulations are now becoming an invaluable tool in complementing experimental findings; a formidable computational and modelling challenge, as they must correctly capture the strong non-Newtonian properties of the flow, by resolving hydrodynamic interactions between different cells and possible variations in the cell concentration over the domain.

% B. Are there any existing solutions?
%		(i) experiments: what has been done so far about blood microrheology:
The establishment of links between the microstructure and macroscopically measured quantities such as the viscosity, is of direct practical interest, as it allowes for predictions of blood flow in the majority of vessels~\supercite{Schmid-Schonbein1971} and medical devices.
Since the 1960s, advances in the field of blood microrheology and microstructure have enabled a number of research groups~\supercite{Chien1970} to report the first established theories regarding the correlation of the RBC microstructure and macroscopically observed quantities.
The viscosity of blood has drawn the attention of researchers, who explained its shear thinning behavior through different theories, based on the hydrodynamic effective volume of the RBCs~\supercite{Chien1970} and the circulation of the enclosed hemoglobin.
The rich dynamics of RBCs were exploited more during the last decades, through experiments of RBCs in shear flows~\supercite{Abkarian2007a}, leading to the identification of unique dynamical movements which are apparent in different shear rate regimes.
However, this behavior is seen for an isolated RBC.
It has been recently shown that at higher RBC concentrations, the RBCs assume a variety of shapes~\supercite{lanotte_red_2016} all of which have an implication on the overall viscosity of the suspension, and in consequence on the blood dynamics.

%		(ii) simulations: Several recent works examined...
% C. Which is the best?
% D. What is its main limitation?
Advancements in the field of computational science and engineering have led to the development of software with the potential to contribute to experimental findings.
The effective integration of mathematical models with data, under the uncertainty quantification framework, has lead to reliable calibration of model parameters~\supercite{wu2016hierarchical}.
The calibration of complex and computationally demanding physical models is becoming possible, through software that are able to exploit the capabilities of massively parallel and hybrid (CPU/GPU) computer architectures~\supercite{hadjidoukas2015pi4u}.
Solvers of grid based and Boundary Integral methods have exhibited excellent scalability~\supercite{Rahimian2010}, and are used for the solution of the linear viscous Stokes equations, with the capability of handling the complex geometries of microfluidic devices.
However, such solvers have limited capabilities in resolving essential sub-micron flow biophysics~\supercite{Freund2014} leading to the overlook of important effects~\supercite{MountrakisLorenzHoekstra2013}, while state-of-the-art boundary integral simulations have only used a few hundred RBCs in two-dimensional geometries.

% 5. What we do...
In this paper we simulate suspensions of RBCs inside rotating cylindrical annuli and observe their characteristics by microstructural statistics.
Our simulations are based on an extension of the Dissipative Particle Dynamics (DPD) method, originally proposed by Hoogerbrugge and Koelman~\supercite{Hoogerbrugge1992}, a particle-based stochastic mesoscopic method that bridges the gap between Molecular Dynamics (MD) and the Navier Stokes equations~\supercite{Groot1997}.
DPD has been extensively used for the modeling of complex systems~\supercite{Li2011}, and has become a key method for the study of blood dynamics with applications in microrheology such as blood microfluidics for cancer therapy~\supercite{Koumoutsakos2013} and particle margination effects used in drug delivery systems~\supercite{Mueller2014}.
State of the art DPD solvers are based on extensions of software packages originally developed for MD simulations, such as LAMMPS~\supercite{Plimpton:1995} and HOOMD-Blue~\supercite{Anderson2008}, with the capability of running on both CPU-only and GPU-accelerated supercomputers.
For our simulations, we use {\it uDeviceX}, a high-throughput open source package with kernels thoroughly optimized for GPUs, for microfluidic simulations using state-of-the-art DPD models~\supercite{Rossinelli:2015}.
We wish to contribute to the efforts towards the accurate modeling of blood microfluidics and provide insights to the dynamics of the microstructural arrangement of the blood constituents, and its implications to the observed macro-rheology.

% 6. This paper is structured as follows...
This paper is structured as follows: Section~\ref{sec:me} provides details for the mathematical model and the boundary conditions between the different combinations of particle interactions.
In Section~\ref{sec:soft}, we briefly describe the software used, along with details on its performance.
In Section~\ref{sec:res}, we specify the simulation setup and perform the necessary validation tests by comparing the output of our DPD simulations to results obtained from simulations using the Boundary Element method.
We present results of high and low hematocrit simulations inside a Taylor-Couette setup, focusing on macro- and micro- scopic flow properties and characteristics.
Our findings are summarized and further discussed in Section~\ref{sec:concl}.
Additional information regarding the simulation setup and parameters are included in the Appendix.
